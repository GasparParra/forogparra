import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";
// Info FireBase
const firebaseConfig = {
    apiKey: "AIzaSyDn4KDPx89a2ROjpGzfylQPYn-kyqVOCYE",
    authDomain: "forogparra.firebaseapp.com",
    projectId: "forogparra",
    storageBucket: "forogparra.appspot.com",
    messagingSenderId: "322378485629",
    appId: "1:322378485629:web:85fad1b9ce3c0dde4d4614"
  };
// Inicializar FireBase
const app = initializeApp(firebaseConfig);
// Conectar con la Base De Datos
const database = getDatabase(app);

console.log("Consola de pruebas...");

// Referencias al HTML
var correoRef = document.getElementById("direccionCorreoId");
var passRef = document.getElementById("passwordId");
var CreateCorreoRef = document.getElementById("CDireccionCorreoId");
var CreatePassRef = document.getElementById("CPasswordId");
var CreatePass2Ref = document.getElementById("CPassword2Id");
var CreateNameRef = document.getElementById("CNameId");
var CreateCityRef = document.getElementById("CCityId");
var buttonRef = document.getElementById("altaButtonId");
var ingresarRef = document.getElementById("ingresarButtonId");

// Event Listeners
buttonRef.addEventListener("click", altaUsuario);
CreateCityRef.addEventListener("keypress", (e) => { 
    if (e.key === 'Enter'){
       altaUsuario();
   }})
ingresarRef.addEventListener("click", logIn);
passRef.addEventListener("keypress", (e) => { 
    if (e.key === 'Enter'){
       logIn();
   }})

// Variables
const auth = getAuth();

// Funciones
function altaUsuario() // Da de alta al usuario
{

    console.log("Ingreso a la función altaUsuario().");

    if((CreateCorreoRef.value != '') && (CreatePassRef.value != '') && (CreatePass2Ref.value != '') && (CreateNameRef.value != '') && (CreateCityRef.value != ''))
    {
        if (CreatePass2Ref.value != CreatePassRef.value)
            {
                alert("Las contraseñas no coinciden")
            }
        else
        {
            createUserWithEmailAndPassword(auth, CreateCorreoRef.value, CreatePassRef.value)
            .then((userCredential) => {
                // Signed in
                const user = userCredential.user;
                console.log("Usuario: " + user + " ID: " + user.uid);
                console.log("Creación de usuario.");
                DataBaseUser(user.uid)

            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
                if(errorCode == 'auth/email-already-in-use'){
                    alert("Mail ya empleado por otro usuario.");
                }
            });
        }
        
    }
    else
    {
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    

}
function DataBaseUser(uid) // Guarda en la base de datos los datos del Usuario
{
    let email = CreateCorreoRef.value
    let nombre = CreateNameRef.value
    let ciudad = CreateCityRef.value
    const UserData = {
        uid:  uid,
        Name: nombre,
        Email: email, 
        city: ciudad,
      };
    CreateCorreoRef.value = ""
    CreateNameRef.value = ""
    CreateCityRef.value = ""
    CreatePass2Ref.value = ""
    CreatePassRef.value = ""
    console.log(UserData)
    console.log(uid)

    set(ref(database, "users/" + uid), {
        UserData
    })
    alert("Carga de usuario correcta.");
}
function logIn () 
{

    if((correoRef.value != '') && (passRef.value != '')){

        signInWithEmailAndPassword(auth, correoRef.value, passRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            window.location.href = "../index.html";
            // ...
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            alert("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    
}
